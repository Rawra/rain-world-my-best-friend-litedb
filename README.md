# rain-world-my-best-friend

<img src="https://gitlab.com/Rawra/rain-world-my-best-friend-litedb/-/raw/main/docs/logo/logo.png" alt="showcase-1" width="600"/>

<img src="https://gitlab.com/Rawra/rain-world-my-best-friend-litedb/-/raw/main/docs/thumbnail.png" alt="showcase-1" width="600"/>

## What it is

This project aims to make tamed lizards not absolute useless.
The goal is to make the time investment a little more worth while, while also
introducing some more rpg-like elements into the game.

## How it works

To achieve this we use the [RW LiteDB API](https://gitlab.com/Rawra/rain-world-database-api-litedb) to
store persistent data alongside the normal save games, as to not touch them directly.

The following is the complete list of functions this mod hooks:

| Class | Function
|-|-
|Lizard|Violence *Pre
|LizardAI|BiteCreature *Pos
|LizardAI|GiftReceived *Pos
|LizardAI|Update *Pos
|LizardAI|AggressiveBehaviour *Pre
|LizardAI|DoIWantToBiteThisCreature *Pre
|ScavengerAI|DontWantToThrowAt *Pre

todo

## Features
Note: Most of these features should still work when using jolly co-op.

- Tamed Lizards will receive scaling damage reduction depending on their level and your bond with them.
- Tamed Lizards will avoid biting each other
- Scavengers will avoid hurting tamed lizards when relationship to the player is good
- Tamed Lizards gain levels and experience by biting other creatures in a fight

## Features: Testing
- Tamed Lizards get exp by spitting
- Tamed Lizards should lose their reputation less quickly (around half of normal decay speed)
- Tamed Lizards need to avoid hunting scavengers with neutral/good relationship to the player

## ToDo: Planned

- Tamed Lizards need to become more competent at helping out a grabbed player
- We need a UI to show the levels of the current tamed lizards on-screen. (InGame stuff or ImGUI API)

## Missing references

This project includes a script to automatically fix missing references added via HintPath in the csproj files.
Just run "update-hintpaths-auto.ps1" and make sure the gameinstalldir.txt contains your correct path to Rain World.

You might want to do this before trying to open the project or compiling.

## Mod Dependencies

The update-hintpaths script might be able to resolve these as long as the mod is installed on your local installation and or workshop.

| Name | Link
|-|-
| Artifact Library | https://gitlab.com/Rawra/rain-world-artifact
| RWDB.LiteDB	   | https://gitlab.com/Rawra/rain-world-database-api-litedb

## Building

If you're not compiling from scratch, it should suffice to clone this repository and just building the sln.
You may want to change gameinstalldir.txt before compiling for quality of life.