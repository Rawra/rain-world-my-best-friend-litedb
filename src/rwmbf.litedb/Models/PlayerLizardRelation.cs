﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWMBF.Models
{
    public class PlayerLizardRelation
    {
        public Player Player { get; private set; }
        public Dictionary<EntityID, AbstractCreature> TamedLizards { get; private set; }

        public PlayerLizardRelation(Player player)
        {
            this.Player = player;
            TamedLizards = new Dictionary<EntityID, AbstractCreature>();
        }
    }
}
