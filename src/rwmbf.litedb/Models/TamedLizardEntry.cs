﻿using RWMBF.LiteDB.Core;
using MoreSlugcats;
using RWDB.LiteDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LiteDB;

namespace RWMBF.LiteDB.Models
{
    public class TamedLizardEntry
    {
        [BsonId(true)]
        public long Id { get; set; }
        [BsonRef("CreatureEntry")]
        public CreatureEntry Creature { get; set; }
        public float Experience { get; set; }
        public int Level { get; set; }

        public TamedLizardEntry(CreatureEntry creature)
        {
            this.Creature = creature;
            this.Experience = 0f;
            this.Level = 1;
        }
        public TamedLizardEntry()
        {

        }

        public void AddExperience(AbstractCreature abstractCreature, float experience)
        {
            Experience += experience;
            if (Experience >= LevelManager.Instance.ExperienceNeededForLevel(Level + 1))
            {
                Globals.mls.LogInfo($"AddExperience: LEVEL UP FOR CREATURE Creature={abstractCreature.ID.number}");
                Level++;

                if (ModManager.MMF && MMF.cfgExtraLizardSounds.Value && (abstractCreature.abstractAI.RealAI as LizardAI).lizard.voice.articulationIndex != MMFEnums.LizardVoiceEmotion.Love.Index)
                    (abstractCreature.abstractAI.RealAI as LizardAI).lizard.voice.MakeSound(MMFEnums.LizardVoiceEmotion.Love, 1f);
            }
            Globals.mls.LogInfo($"AddExperience: creature={abstractCreature.ID.number} exp={Experience}");
        }
    }
}
