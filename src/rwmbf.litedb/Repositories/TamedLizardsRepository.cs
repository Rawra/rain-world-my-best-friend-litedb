﻿using RWMBF.Models;
using RWDB.LiteDB.Core;
using RWDB.LiteDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optional;
using RWDB.LiteDB.Enums;
using RWMBF.LiteDB.Core;
using RWMBF.LiteDB.Models;
namespace RWMBF.LiteDB.Repository
{
    /// <summary>
    /// Repository
    /// Implements CRUD Operations
    /// </summary>
    public partial class TamedLizardsRepositoryLiteDB : ITamedLizardsRepository<TamedLizardEntry>
    {
        // == Add/Update/Remove ==

        public Option<TamedLizardEntry> Add(TamedLizardEntry value)
        {
            // Add tamed lizard
            if ((DatabaseAPI.Instance.TamedLizards.Insert(value)).IsNull)
                return Option.None<TamedLizardEntry>();

            return Option.Some<TamedLizardEntry>(value);
        }

        public Option<TamedLizardEntry> AddIfNotExists(TamedLizardEntry value)
        {
            if (DatabaseAPI.Instance.TamedLizards
                .Include(x => x.Creature)
                .Exists(x => x.Creature.Id == value.Creature.Id))
                return Option.None<TamedLizardEntry>();

            return Add(value);
        }

        public DBRepositoryResult AddOrUpdate(TamedLizardEntry value)
        {
            var result = AddIfNotExists(value);
            if (result.HasValue)
                return DBRepositoryResult.Updated | DBRepositoryResult.Success;

            if (TryUpdate(value) != DBRepositoryResult.Success)
                return DBRepositoryResult.Error;

            return DBRepositoryResult.Updated | DBRepositoryResult.Success;
        }

        public DBRepositoryResult TryUpdate(TamedLizardEntry value)
        {
            if (DatabaseAPI.Instance.TamedLizards.Update(value))
                return DBRepositoryResult.Updated | DBRepositoryResult.Success;

            return DBRepositoryResult.Error;
        }

        public DBRepositoryResult TryRemove(TamedLizardEntry value)
        {
            if (DatabaseAPI.Instance.TamedLizards.Delete(value.Id))
                return DBRepositoryResult.Deleted | DBRepositoryResult.Success;

            return DBRepositoryResult.Error;
        }

        // == Retrieve ==

        public IEnumerable<TamedLizardEntry> GetAll()
        {
            return DatabaseAPI.Instance.TamedLizards.FindAll();
        }

        public Option<TamedLizardEntry> GetById(long id)
        {
            TamedLizardEntry value = DatabaseAPI.Instance.TamedLizards.FindById(id);
            if (value == null)
                return Option.None<TamedLizardEntry>();
            
            return Option.Some<TamedLizardEntry>(value);
        }

        public Option<TamedLizardEntry> GetByCreature(CreatureEntry creature)
        {
            TamedLizardEntry value = (DatabaseAPI.Instance.TamedLizards
                .Include(x => x.Creature)
                .Find(x => x.Creature.Id == creature.Id)).FirstOrDefault();

            if (value == default)
                return Option.None<TamedLizardEntry>();

            return Option.Some<TamedLizardEntry>(value);
        }

    }

}
