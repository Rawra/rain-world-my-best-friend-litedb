﻿using RWMBF.Models;
using Optional;
using RWDB.LiteDB.Enums;
using RWDB.LiteDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWMBF.LiteDB.Repository
{
    /// <summary>
    /// Interface: Repository for CreatureRepository
    /// Defines CRUD Methods
    /// </summary>
    public interface ITamedLizardsRepository<T>
    {
        Option<T> Add(T value);
        Option<T> AddIfNotExists(T value);
        DBRepositoryResult AddOrUpdate(T value);
        DBRepositoryResult TryUpdate(T value);
        DBRepositoryResult TryRemove(T value);
        IEnumerable<T> GetAll();
        Option<T> GetById(long id);
        Option<T> GetByCreature(CreatureEntry creature);
    }

}
