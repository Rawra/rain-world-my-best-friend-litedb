﻿using MoreSlugcats;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWMBF.LiteDB.Core
{
    public static class LizardHelper
    {
        /// <summary>
        /// Gets an existing relationship from Creature x to Entity y
        /// </summary>
        /// <param name="creature"></param>
        /// <param name="id"></param>
        /// <param name="relationShip"></param>
        /// <returns></returns>
        public static bool GetRelationshipToId(this Creature creature, EntityID id, out SocialMemory.Relationship relationShip)
        {
            List<SocialMemory.Relationship> relationShips = creature.State.socialMemory.relationShips;
            for (int j = 0; j < relationShips.Count; j++)
            {
                if (relationShips[j].subjectID == id)
                {
                    relationShip = relationShips[j];
                    return true;
                }
            }
            relationShip = null;
            return false;
        }

        // TODO: Make it always go one more level.
        public static void BuffLizard(Lizard lizard)
        {
            Globals.mls.LogInfo($"BuffLizard: buffing lizard, id={lizard.abstractCreature.ID}, type={lizard.Template.type.value}");
            // Base stats increase
            lizard.Template.bodySize *= 1.2f;
            lizard.Template.damageRestistances[(int)Creature.DamageType.Explosion, 0] *= 1.5f;
            lizard.Template.damageRestistances[(int)Creature.DamageType.Bite, 0] *= 1.5f;
            lizard.Template.damageRestistances[(int)Creature.DamageType.Bite, 1] *= 1.5f;
            lizard.Template.baseDamageResistance *= 1.5f;
            lizard.Template.baseStunResistance *= 1.5f;

            // Tile Resistances (each creatures gains one new movement type)
            List<TileTypeResistance> tileResistances = new List<TileTypeResistance>();
            List<TileConnectionResistance> connectionResistances = new List<TileConnectionResistance>();

            // Gains: Wall Climb
            if (lizard.Template.type == CreatureTemplate.Type.PinkLizard)
            {
                ((LizardBreedParams)lizard.Template.breedParameters).terrainSpeeds[4] = new LizardBreedParams.SpeedMultiplier(1f, 1f, 1f, 1f);
                tileResistances.Add(new TileTypeResistance(AItile.Accessibility.Wall, 1f, PathCost.Legality.Allowed));

                //connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 40f, PathCost.Legality.Allowed));
            }

            // Gains: Pole climbing
            if (lizard.Template.type == CreatureTemplate.Type.GreenLizard)
            {
                ((LizardBreedParams)lizard.Template.breedParameters).terrainSpeeds[3] = new LizardBreedParams.SpeedMultiplier(0.5f, 1f, 0.75f, 1f);
                tileResistances.Add(new TileTypeResistance(AItile.Accessibility.Climb, 2.5f, PathCost.Legality.Allowed));

                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 40f, PathCost.Legality.Allowed));

                //connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToFloor, 40f, PathCost.Legality.Allowed));
                //connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.LizardTurn, 60f, PathCost.Legality.Allowed));
            }

            // Gains: Nothing
            if (lizard.Template.type == CreatureTemplate.Type.BlueLizard)
            {
                // Nothing to be done here :D
                /*
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToFloor, 20f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 2f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ShortCut, 15f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachOverGap, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachUp, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachDown, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.CeilingSlope, 2f, PathCost.Legality.Allowed));
                */
            }

            // Gains: Wall Climb
            if (lizard.Template.type == CreatureTemplate.Type.YellowLizard)
            {

                ((LizardBreedParams)lizard.Template.breedParameters).terrainSpeeds[4] = new LizardBreedParams.SpeedMultiplier(1f, 1f, 1f, 1f);
                tileResistances.Add(new TileTypeResistance(AItile.Accessibility.Wall, 1f, PathCost.Legality.Allowed));

                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ShortCut, 15f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachOverGap, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachUp, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachDown, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.CeilingSlope, 2f, PathCost.Legality.Allowed));

                //connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToFloor, 4f, PathCost.Legality.Allowed));
                //connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 8f, PathCost.Legality.Allowed));
            }

            // Gains: Nothing
            if (lizard.Template.type == CreatureTemplate.Type.WhiteLizard)
            {
                // Nothing to be done here :D
                /*
                 * connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToFloor, 10f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 10f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ShortCut, 2f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachOverGap, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachUp, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachDown, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.CeilingSlope, 20f, PathCost.Legality.Allowed));
                */
            }

            // Gains: Wall Climb
            if (lizard.Template.type == CreatureTemplate.Type.RedLizard)
            {
                ((LizardBreedParams)lizard.Template.breedParameters).terrainSpeeds[4] = new LizardBreedParams.SpeedMultiplier(1f, 1f, 1f, 1f);
                tileResistances.Add(new TileTypeResistance(AItile.Accessibility.Wall, 1f, PathCost.Legality.Allowed));

                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ShortCut, 15f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachOverGap, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachUp, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachDown, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.CeilingSlope, 2f, PathCost.Legality.Allowed));
            }

            // Gains: Wall Climb
            if (lizard.Template.type == CreatureTemplate.Type.BlackLizard)
            {
                ((LizardBreedParams)lizard.Template.breedParameters).terrainSpeeds[4] = new LizardBreedParams.SpeedMultiplier(1f, 1f, 1f, 1f);
                tileResistances.Add(new TileTypeResistance(AItile.Accessibility.Wall, 1f, PathCost.Legality.Allowed));

                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ShortCut, 15f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachOverGap, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachUp, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachDown, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.CeilingSlope, 2f, PathCost.Legality.Allowed));
            }

            // Gains: Wall Climb
            if (lizard.Template.type == CreatureTemplate.Type.Salamander)
            {
                ((LizardBreedParams)lizard.Template.breedParameters).terrainSpeeds[4] = new LizardBreedParams.SpeedMultiplier(1f, 1f, 1f, 1f);
                tileResistances.Add(new TileTypeResistance(AItile.Accessibility.Wall, 1f, PathCost.Legality.Allowed));

                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ShortCut, 15f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachOverGap, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachUp, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachDown, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.CeilingSlope, 2f, PathCost.Legality.Allowed));

                //connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 40f, PathCost.Legality.Allowed));
            }

            // Gains: Nothing
            if (lizard.Template.type == CreatureTemplate.Type.CyanLizard)
            {
                // Nothing to be done here :D
                /*
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToFloor, 2f, PathCost.Legality.Allowed));
			    connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 2f, PathCost.Legality.Allowed));
			    connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ShortCut, 5f, PathCost.Legality.Allowed));
			    connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachOverGap, 1.1f, PathCost.Legality.Allowed));
			    connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachUp, 1.1f, PathCost.Legality.Allowed));
			    connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachDown, 1.1f, PathCost.Legality.Allowed));
			    connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.CeilingSlope, 2f, PathCost.Legality.Allowed));
                */
            }

            // Gains: Pole Climb
            if (ModManager.MSC && lizard.Template.type == MoreSlugcatsEnums.CreatureTemplateType.SpitLizard)
            {
                ((LizardBreedParams)lizard.Template.breedParameters).terrainSpeeds[3] = new LizardBreedParams.SpeedMultiplier(0.5f, 1f, 0.75f, 1f);
                tileResistances.Add(new TileTypeResistance(AItile.Accessibility.Climb, 2.5f, PathCost.Legality.Allowed));

                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 40f, PathCost.Legality.Allowed));

                //connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToFloor, 40f, PathCost.Legality.Allowed));
                //connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.LizardTurn, 60f, PathCost.Legality.Allowed));
            }

            // Gains: Wall Climb
            if (ModManager.MSC && lizard.Template.type == MoreSlugcatsEnums.CreatureTemplateType.ZoopLizard)
            {
                ((LizardBreedParams)lizard.Template.breedParameters).terrainSpeeds[4] = new LizardBreedParams.SpeedMultiplier(1f, 1f, 1f, 1f);
                tileResistances.Add(new TileTypeResistance(AItile.Accessibility.Wall, 1f, PathCost.Legality.Allowed));

                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ShortCut, 15f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachOverGap, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachUp, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachDown, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.CeilingSlope, 2f, PathCost.Legality.Allowed));

                //list2.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToFloor, 10f, PathCost.Legality.Allowed));
                //list2.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 0.2f, PathCost.Legality.Allowed));
            }

            // Gains: Wall Climb
            if (ModManager.MSC && lizard.Template.type == MoreSlugcatsEnums.CreatureTemplateType.TrainLizard)
            {
                ((LizardBreedParams)lizard.Template.breedParameters).terrainSpeeds[4] = new LizardBreedParams.SpeedMultiplier(1f, 1f, 1f, 1f);
                tileResistances.Add(new TileTypeResistance(AItile.Accessibility.Wall, 1f, PathCost.Legality.Allowed));

                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ShortCut, 15f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachOverGap, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachUp, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.ReachDown, 1.1f, PathCost.Legality.Allowed));
                connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.CeilingSlope, 2f, PathCost.Legality.Allowed));

                //connectionResistances.Add(new TileConnectionResistance(MovementConnection.MovementType.DropToClimb, 4f, PathCost.Legality.Allowed));
            }


            // Add the tile resistances to the pathing prefs
            for (int l = 0; l < tileResistances.Count; l++)
            {
                lizard.Template.pathingPreferencesTiles[(int)tileResistances[l].accessibility] = tileResistances[l].cost;
                if (tileResistances[l].cost.legality == PathCost.Legality.Allowed && lizard.Template.maxAccessibleTerrain < (int)tileResistances[l].accessibility)
                {
                    lizard.Template.maxAccessibleTerrain = (int)tileResistances[l].accessibility;
                }
            }

            // Add the resistance pathing settings
            for (int n = 0; n < connectionResistances.Count; n++)
            {
                lizard.Template.pathingPreferencesConnections[(int)connectionResistances[n].movementType] = connectionResistances[n].cost;
            }

            //__instance.Template.GetType().GetConstructor(Type.EmptyTypes).Invoke(__instance.Template, new object[] { });
        }

        public static bool LikesPlayer(Player pl, Lizard lz)
        {
            return lz.AI.LikeOfPlayer(lz.AI.tracker.RepresentationForCreature(pl.abstractCreature, false)) > 0.5f;
        }
    }
}
