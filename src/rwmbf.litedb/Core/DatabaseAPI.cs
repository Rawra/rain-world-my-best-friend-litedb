﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LiteDB;
using RWMBF.LiteDB.Models;
using RWDB.LiteDB.Core;
namespace RWMBF.LiteDB.Core
{
    internal class DatabaseAPI
    {
        private static readonly Lazy<DatabaseAPI> lazy = new Lazy<DatabaseAPI>(() => new DatabaseAPI());
        public static DatabaseAPI Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        private DatabaseAPI()
        {

        }

        /// <summary>
        /// LiteDB in-memory collections
        /// </summary>
        public ILiteCollection<TamedLizardEntry> TamedLizards { get; private set; }

        /// <summary>
        /// Initiates the database session
        /// </summary>
        public bool TrySessionSetup()
        {
            Globals.mls.LogInfo("TrySessionSetup()");

            if (!TrySetupTables())
            {
                Globals.mls.LogInfo("TrySessionSetup() - Error while creating tables!");
                return false;
            }

            Globals.mls.LogInfo("TrySessionSetup() - OK");
            return true;
        }

        /// <summary>
        /// Creates all the tables for an "initial" state of db
        /// </summary>
        private bool TrySetupTables()
        {
            Globals.mls.LogInfo("TrySetupTables()");

            TamedLizards = DapperManager.Instance.LiteDatabase.GetCollection<TamedLizardEntry>();

            // Setup collection indexes
            TamedLizards.EnsureIndex(x => x.Creature);

            Globals.mls.LogInfo("TrySetupTables() - OK");
            return true;
        }

    }
}
