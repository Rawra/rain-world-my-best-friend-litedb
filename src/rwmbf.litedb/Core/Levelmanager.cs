﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RWMBF.LiteDB.Core
{
    internal class LevelManager
    {
        private static readonly Lazy<LevelManager> lazy = new Lazy<LevelManager>(() => new LevelManager());
        public static LevelManager Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        private LevelManager()
        {
            LevelExperienceTable = new float[LevelMax];
            PopulateExperienceTable();
        }

        public const int LevelMax = 20 + 1;
        public float[] LevelExperienceTable;
        public static class ExperienceGain
        {
            public const float Bite = 1.0f;
            public const float DamageReceived = 0.25f;
            public const float Spit = 0.25f;
        }

        private void PopulateExperienceTable()
        {
            Globals.mls.LogInfo("PopulateExperienceTable");
            for (int i = 0; i < LevelMax; i++)
            {
                LevelExperienceTable[i] = ExperienceNeededForLevel(i);
                Globals.mls.LogInfo($"Level\t[{i}]:\t{LevelExperienceTable[i]}");
            }
        }

        public float ExperienceNeededForLevel(float level)
        {
            return Mathf.Pow(level / 0.8f, 2f);
        }
    }
}
