﻿using BepInEx.Bootstrap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWMBF.LiteDB.Core
{
    internal sealed class BepInExUtil
    {
        // Function to check if a mod with the given GUID is loaded
        public static bool IsModLoaded(string modGUID)
        {
            foreach (var dependency in Chainloader.PluginInfos)
            {
                if (dependency.Value.Metadata.GUID == modGUID)
                    return true;
            }
            return false;
        }

    }
}
