﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RWMBF.LiteDB.Models;
using RWDB.LiteDB.Models;
using Optional;
using Optional.Unsafe;
using RWMBF.LiteDB.Repository;

namespace RWMBF.LiteDB.Core
{
    internal class TamedLizardManager
    {
        private static readonly Lazy<TamedLizardManager> lazy = new Lazy<TamedLizardManager>(() => new TamedLizardManager());
        public static TamedLizardManager Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        private TamedLizardManager()
        {
            tamedLizardsRepository = new TamedLizardsRepositoryLiteDB();
        }

        /// <summary>
        /// CRUD Repositories
        /// </summary>
        private TamedLizardsRepositoryLiteDB tamedLizardsRepository;
        public TamedLizardsRepositoryLiteDB TamedLizardsRepository
        {
            get { return tamedLizardsRepository; }
            private set { tamedLizardsRepository = value; }
        }

        /// <summary>
        /// Add a tamed lizard to the database
        /// </summary>
        /// <param name="creature"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public bool TryAddTamedLizard(AbstractCreature creature, uint instanceId)
        {
            Globals.mls.LogInfo($"TryAddTamedLizard: creature={creature.ID.number}, instanceId={instanceId}");

            // Get the current save
            Option<SaveEntry> save = RWDB.LiteDB.Core.SaveDataManager.Instance.SavesRepository.GetByChecksum(instanceId);
            if (!save.HasValue)
            {
                Globals.mls.LogInfo($"TryAddTamedLizard: GetByChecksum returned null");
                return false;
            }

            // Add the creature
            CreatureEntry creatureEntry = new CreatureEntry(creature);
            Option<CreatureEntry> addedCreature = RWDB.LiteDB.Core.SaveDataManager.Instance.CreatureRepository.AddIfNotExists(save.ValueOrFailure(), creatureEntry);
            if (!addedCreature.HasValue)
            {
                Globals.mls.LogInfo($"TryAddTamedLizard: CreatureRepository.AddIfNotExists -- returned false");
                return false;
            }
            Globals.mls.LogInfo($"TryAddTamedLizard: CreatureRepository.AddIfNotExists, added id={creatureEntry.Id}");

            // Add the tamed lizard
            TamedLizardEntry tamedLizardEntry = new TamedLizardEntry(creatureEntry);
            Option<TamedLizardEntry> addedTamedLizard = TamedLizardsRepository.AddIfNotExists(tamedLizardEntry);
            if (!addedTamedLizard.HasValue)
            {
                Globals.mls.LogInfo($"TryAddTamedLizard: TamedLizardsRepository.AddByCreatureIfNotExists -- returned false");
                return false;
            }
            Globals.mls.LogInfo($"TryAddTamedLizard: TamedLizardsRepository.AddByCreatureIfNotExists, added id={creatureEntry.Id}");

            return true;
        }

        /// <summary>
        /// Remove a tamed lizard from the database
        /// </summary>
        /// <param name="creature"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public bool TryRemoveTamedLizard(AbstractCreature creature, uint instanceId)
        {
            Globals.mls.LogInfo($"TryRemoveTamedLizard: creature={creature.ID.number}, instanceId={instanceId}");

            // Get the tamed lizard
            Option<TamedLizardEntry> tamedLizard = GetTamedLizard(creature, instanceId);
            if (!tamedLizard.HasValue)
            {
                Globals.mls.LogInfo($"TryRemoveTamedLizard: GetTamedLizard -- returned null");
                return false;
            }

            // Remove the tamed lizard
            TamedLizardsRepository.TryRemove(tamedLizard.ValueOrFailure());

            return true;
        }

        /// <summary>
        /// Retrieve a tamed lizard from the database
        /// </summary>
        /// <param name="creature"></param>
        /// <param name="instanceId"></param>
        /// <param name="tamedLizardData"></param>
        /// <returns></returns>
        public Option<TamedLizardEntry> GetTamedLizard(AbstractCreature creature, uint instanceId)
        {
            //Globals.mls.LogInfo($"GetTamedLizard: creature={creature.ID.number}, instanceId={instanceId}");

            // Get the current save
            Option<SaveEntry> save = RWDB.LiteDB.Core.SaveDataManager.Instance.SavesRepository.GetByChecksum(instanceId);
            if (!save.HasValue)
            {
                Globals.mls.LogInfo($"GetTamedLizard: GetByChecksum returned null");
                return Option.None<TamedLizardEntry>();
            }

            // Get the creature
            Option<CreatureEntry> retrievedCreature = RWDB.LiteDB.Core.SaveDataManager.Instance.CreatureRepository.GetByGameId(save.ValueOrFailure(), creature.ID.number);
            if (!retrievedCreature.HasValue)
            {
                Globals.mls.LogInfo($"GetTamedLizard: CreatureRepository.GetByGameId -- returned null");
                return Option.None<TamedLizardEntry>();
            }

            // Get the tamed lizard
            Option<TamedLizardEntry> tamedLizard = TamedLizardsRepository.GetByCreature(retrievedCreature.ValueOrFailure());
            return tamedLizard;
        }

        /// <summary>
        /// Add experience to a tamed lizard
        /// </summary>
        /// <param name="creature"></param>
        /// <param name="instanceId"></param>
        /// <param name="experience"></param>
        /// <returns></returns>
        public bool TryAddExperience(AbstractCreature creature, uint instanceId, float experience)
        {
            Globals.mls.LogInfo($"TryAddExperience: {creature.ID} instance={instanceId} exp={experience}");

            // Get the tamed lizard
            Option<TamedLizardEntry> tamedLizard = GetTamedLizard(creature, instanceId);
            if (!tamedLizard.HasValue)
            {
                Globals.mls.LogInfo($"TryAddExperience: GetTamedLizard -- returned null");
                return false;
            }

            // Add exp
            tamedLizard.ValueOrFailure().AddExperience(creature, experience);

            // Save
            TamedLizardsRepository.TryUpdate(tamedLizard.ValueOrFailure());

            return true;
        }
    }
}
