﻿using RWMBF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWMBF.LiteDB.Core
{
    public sealed class PlayerLizardRelations
    {
        private static readonly Lazy<PlayerLizardRelations> lazy = new Lazy<PlayerLizardRelations>(() => new PlayerLizardRelations());
        public static PlayerLizardRelations Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        private PlayerLizardRelations()
        {

        }

        /// <summary>
        /// Contains all player->lizard relations (one-to-many)
        /// </summary>
        public List<PlayerLizardRelation> Relations { get; private set; } = new List<PlayerLizardRelation>();

        /// <summary>
        /// Try to add a lizard to the tamed lizards book keeping (Player class var)
        /// </summary>
        /// <param name="player"></param>
        /// <param name="creature"></param>
        /// <returns></returns>
        public bool TryAddPlayerTamedLizard(Player player, AbstractCreature creature)
        {
            return TryAddPlayerTamedLizard(player.abstractCreature.ID, creature);
        }

        /// <summary>
        /// Try to add a lizard to the tamed lizards book keeping (Player ID var)
        /// </summary>
        /// <param name="playerID"></param>
        /// <param name="creature"></param>
        /// <returns></returns>
        public bool TryAddPlayerTamedLizard(EntityID playerID, AbstractCreature creature)
        {
            // Check if a player lizard relation exists with said player
            if (!Relations.Exists(x => x.Player.abstractCreature.ID == playerID))
            {
                Globals.mls.LogDebug("TryAddPlayerTamedLizard: Can't find player by id. Skipping...");
                return false;
            }

            PlayerLizardRelation splr = Relations.FirstOrDefault(x => x.Player.abstractCreature.ID == playerID);
            if (splr == default)
            {
                Globals.mls.LogError($"TryAddPlayerTamedLizard: player-lizard relation received is default!");
                return false;
            }

            // Only add a tamed lizard once
            if (splr.TamedLizards.ContainsKey(creature.ID))
            {
                Globals.mls.LogDebug($"TryAddPlayerTamedLizard: player-lizard relation: player: {playerID}, creature: {creature.ID} -- lizard already added to tamedlizards list");
                return false;
            }

            splr.TamedLizards.Add(creature.ID, creature);
            return true;
        }

        /// <summary>
        /// Try to remove a player tamed lizard from book keeping
        /// </summary>
        /// <param name="playerID"></param>
        /// <param name="creature"></param>
        /// <returns></returns>
        public bool TryRemovePlayerTamedLizard(EntityID playerID, AbstractCreature creature)
        {
            PlayerLizardRelation x = Relations.FirstOrDefault(y => y.Player.abstractCreature.ID == playerID);
            if (x == default)
                return false;

            x.TamedLizards.Remove(creature.ID);
            return true;
        }

        /// <summary>
        /// Check if a given lizard is tamed by their abstract class instance
        /// </summary>
        /// <param name="creature"></param>
        /// <returns></returns>
        public bool IsTamedLizard(AbstractCreature creature)
        {
            return IsTamedLizard(creature.ID);
        }

        /// <summary>
        /// Check if a given lizard is tamed by their id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsTamedLizard(EntityID id)
        {
            PlayerLizardRelation x = Relations.FirstOrDefault(y => y.TamedLizards.ContainsKey(id));
            if (x == default)
                return false;

            return true;
        }

        /// <summary>
        /// Check if a given entity is a player (check against known player lizard relations)
        /// </summary>
        /// <param name="entityID"></param>
        /// <returns></returns>
        public bool IsEntityIDPlayer(EntityID entityID)
        {
            if (PlayerLizardRelations.Instance.Relations.Exists(x => x.Player.abstractCreature.ID == entityID))
                return true;

            return false;
        }
    }
}
