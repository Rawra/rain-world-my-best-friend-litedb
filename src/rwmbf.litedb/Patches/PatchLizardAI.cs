﻿using HarmonyLib;
using Menu;
using MoreSlugcats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using RWDB.LiteDB.Core;
using RWMBF.LiteDB.Core;
namespace RWMBF.LiteDB.Patches
{

    /// <summary>
    /// MBF Feature: When a tamed lizard bites something, it will gain experience
    /// </summary>
    [HarmonyPatch(typeof(LizardAI), nameof(LizardAI.BitCreature))]
    public sealed class PatchLizardAI_BiteCreature
    {
        private static void Postfix(LizardAI __instance, BodyChunk chunk)
        {
            if (!PlayerLizardRelations.Instance.IsTamedLizard(__instance.creature))
                return;

            // Add experience to this lizard
            //CreatureDataManager.Instance.TryAddExperience(__instance.creature, RWDB.Globals.WorldInstance, ExperienceGain.Bite);
        }
    }

    /// <summary>
    /// MBF Feature: Add newly tamed lizards to our book keeping of tamed ones.
    /// </summary>
    [HarmonyPatch(typeof(LizardAI), nameof(LizardAI.GiftRecieved))]
    public sealed class PatchLizardAI_GiftReceived
    {
        private static void Postfix(LizardAI __instance, SocialEventRecognizer.OwnedItemOnGround giftOfferedToMe)
        {
            //Globals.mls.LogInfo("LIZARDAI-GiftReceived: POSTFIX RUN");

            // Check if friends now with a feeding player
            if (__instance.friendTracker.friend != null && !PlayerLizardRelations.Instance.IsTamedLizard(__instance.creature))
            {
                EntityID feedingPlayerID = __instance.friendTracker.friend.abstractCreature.ID;

                Globals.mls.LogInfo($"LIZARDAI-GiftReceived: Adding new friend, player={feedingPlayerID.number} :)");
                if (__instance.friendTracker.friend is Player)
                    PlayerLizardRelations.Instance.TryAddPlayerTamedLizard(__instance.friendTracker.friend as Player, __instance.creature);
            }

            if (__instance.creature.realizedCreature.GetRelationshipToId(giftOfferedToMe.owner.abstractCreature.ID, out SocialMemory.Relationship relation))
            {
                Globals.mls.LogInfo($"LIZARDAI-GiftReceived: new lizard relation: like={relation.like}, tempLike={relation.tempLike}, lizard={__instance.creature.ID}");
            }
        }
    }

    /// <summary>
    /// MBF Feature: Every update, we see if the relationship to a player has become "untame" and then we remove it from the 
    /// known tamed lizards list
    /// </summary>
    [HarmonyPatch(typeof(LizardAI), nameof(LizardAI.Update))]
    public sealed class PatchLizardAI_Update
    {
        private const float likeThreshold = 0.6f;       // Default: 0.5f
        private const float dislikeThreshold = 0.4f;    // Default: 0.5f
        private const float healCooldown = 30f;
        private const float heal = 0.1f;                // 10%
        private static readonly Dictionary<EntityID, float> healTimer = new Dictionary<EntityID, float>();

        private static void Postfix(LizardAI __instance)
        {
            // Check periodically if we are tamed still (>0.5 is tamed, <0.5 untamed)
            //Globals.mls.LogInfo($"LIZARDAI-Update: LIZARD {__instance.creature.ID} RELATIONS n={__instance.creature.state.socialMemory.relationShips.Count}");

            foreach (SocialMemory.Relationship r in __instance.creature.state.socialMemory.relationShips)
            {
                //Globals.mls.LogInfo($"LIZARDAI-Update: like={r.like}, tempLike={r.tempLike}, to_subject_id={r.subjectID}");

                // Check if we are considered "tamed"
                if (
                    (r.like >= likeThreshold && r.tempLike >= likeThreshold) &&
                     PlayerLizardRelations.Instance.IsEntityIDPlayer(r.subjectID) &&
                    !PlayerLizardRelations.Instance.IsTamedLizard(__instance.creature))
                {
                    Globals.mls.LogInfo($"LIZARDAI-Update: adding new tamed lizard, id={__instance.creature.ID}, due to like " + r.like + ", tLike" + r.tempLike);
                    if (!PlayerLizardRelations.Instance.TryAddPlayerTamedLizard(r.subjectID, __instance.creature))
                    {
                        Globals.mls.LogInfo($"LIZARDAI-Update: adding new tamed lizard, id={__instance.creature.ID} -- error");
                        continue;
                    }
                    Globals.mls.LogInfo($"LIZARDAI-Update: adding new tamed lizard, id={__instance.creature.ID} -- ok");

                    TamedLizardManager.Instance.TryAddTamedLizard(__instance.creature, SaveDataManager.Instance.WorldInstance);
                    continue;
                }

                if (
                    (r.like <= dislikeThreshold && r.tempLike <= dislikeThreshold) &&
                    PlayerLizardRelations.Instance.IsEntityIDPlayer(r.subjectID) &&
                    PlayerLizardRelations.Instance.IsTamedLizard(__instance.creature))
                {
                    Globals.mls.LogInfo($"LIZARDAI-Update: removing tamed lizard, id={__instance.creature.ID}, due to like " + r.like + ", tLike" + r.tempLike);
                    if (!PlayerLizardRelations.Instance.TryRemovePlayerTamedLizard(r.subjectID, __instance.creature))
                    {
                        Globals.mls.LogInfo($"LIZARDAI-Update: removing tamed lizard, id={__instance.creature.ID} -- error");
                        continue;
                    }
                    Globals.mls.LogInfo($"LIZARDAI-Update: removing tamed lizard, id={__instance.creature.ID} -- ok");

                    TamedLizardManager.Instance.TryRemoveTamedLizard(__instance.creature, SaveDataManager.Instance.WorldInstance);
                    continue;
                }
            }

            // Heal ourselves if we are a tamed lizard
            EntityID creatureId = __instance.creature.ID;
            float creatureHealth = ((LizardState)__instance.creature.realizedCreature.State).health;
            if (PlayerLizardRelations.Instance.IsTamedLizard(__instance.creature) && !Mathf.Approximately(creatureHealth, 1.0f))
            {
                if (!healTimer.ContainsKey(creatureId))
                    healTimer.Add(creatureId, 0);

                healTimer[creatureId] += Time.deltaTime;
                if (healTimer[creatureId] >= healCooldown)
                {
                    // Reset the timer
                    healTimer[creatureId] = 0f;

                    // Heal the lizard
                    creatureHealth = Mathf.Max(creatureHealth + heal, 1.0f);
                    ((LizardState)__instance.creature.realizedCreature.State).health = creatureHealth;

                    // Play heal sound
                    if (ModManager.MMF && MMF.cfgExtraLizardSounds.Value && __instance.lizard.voice.articulationIndex != MMFEnums.LizardVoiceEmotion.Love.Index)
                        __instance.lizard.voice.MakeSound(MMFEnums.LizardVoiceEmotion.Love, 1f);

                    //Globals.mls.LogInfo($"LIZARDAI-Update: healing tamed lizard, id={__instance.creature.ID}, hp={creatureHealth}");
                }
            }
        }
    }

    /// <summary>
    /// MBF Feature: Don't show aggressive behaviour to other tamed lizards
    /// </summary>
    [HarmonyPatch(typeof(LizardAI), nameof(LizardAI.AggressiveBehavior))]
    public sealed class PatchLizardAI_AggressiveBehaviour
    {
        private static bool Prefix(LizardAI __instance, Tracker.CreatureRepresentation target, float tongueChance)
        {
            if (
            PlayerLizardRelations.Instance.IsTamedLizard(__instance.creature) &&
            target.representedCreature.realizedCreature is Lizard &&
            PlayerLizardRelations.Instance.IsTamedLizard(target.representedCreature))
            {
                //Globals.mls.LogInfo($"LIZARDAI-AggressiveBehavior: None please. Source={__instance.creature.ID}, Target={target.representedCreature.ID}");
                return false;
            }
            return true;
        }
    }

    /// <summary>
    /// MBF Feature: Try avoid biting other friendly tamed lizards
    /// </summary>
    [HarmonyPatch(typeof(LizardAI), nameof(LizardAI.DoIWantToBiteThisCreature))]
    public sealed class PatchLizardAI_DoIWantToBiteThisCreature
    {
        private static bool Prefix(LizardAI __instance, ref bool __result, Tracker.CreatureRepresentation otherCrit)
        {
            //Globals.mls.LogInfo($"LIZARDAI-DoIWantToBiteThisCreature: PREFIX RUN, id: {__instance.creature.ID}, otherCrit: {otherCrit.representedCreature.ID}");

            // If we are not a tamed lizard, we don't continue here
            if (!PlayerLizardRelations.Instance.IsTamedLizard(__instance.creature))
                return true;

            // Feature: avoid biting other friendly tamed lizards
            // Check if we are a tamed lizard
            // Check if the other creature is a lizard
            // Check if the other creature is a tamed lizard
            if (
            otherCrit.representedCreature.realizedCreature is Lizard &&
            PlayerLizardRelations.Instance.IsTamedLizard(otherCrit.representedCreature))
            {
                //Globals.mls.LogInfo($"LIZARDAI-DoIWantToBiteThisCreature: No, I don't. Source={__instance.creature.ID}, Target={otherCrit.representedCreature.ID}");
                __result = false;
                return false;
            }
            
            // Feature: avoid biting neutral or friendly scavengers
            if (otherCrit.representedCreature.realizedCreature is Scavenger)
            {
                if (!otherCrit.representedCreature.realizedCreature.GetRelationshipToId(__instance.friendTracker.friend.abstractCreature.ID, out SocialMemory.Relationship scavRelToPlayer))
                    return true;

                if (scavRelToPlayer.like < 0.6f || scavRelToPlayer.tempLike < 0.6f)
                    return true;

                __result = false;
                return false;
            }

            return true;
        }
    }
}
