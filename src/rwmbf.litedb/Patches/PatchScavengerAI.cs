﻿using BepInEx.Logging;
using HarmonyLib;
using Menu;
using MoreSlugcats;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using RWMBF.LiteDB.Core;
namespace RWMBF.LiteDB.Patches
{

    /// <summary>
    /// Try to avoid throwing spears at friendly tamed lizards (player ones only!)
    /// </summary>
    [HarmonyPatch(typeof(ScavengerAI), nameof(ScavengerAI.DontWantToThrowAt))]
    public sealed class PatchScavengerAI_DontWantToThrowAt
    {
        private static bool Prefix(ScavengerAI __instance, ref bool __result, Tracker.CreatureRepresentation rep)
        {
            //Globals.mls.LogInfo("PatchScavengerAI_DontWantToThrowAt: PREFIX RUN");

            // Check if the creature is a Lizard
            if (!(rep.representedCreature.realizedCreature is Lizard))
                return true;

            // Check if it is a tamed lizard
            if (!PlayerLizardRelations.Instance.IsTamedLizard(rep.representedCreature))
                return true;

            // Get all creatures of the social memory of this scavenger
            ref List<SocialMemory.Relationship> relationShips = ref __instance.creature.state.socialMemory.relationShips;

            // Get all player social memories of this scavenger
            var playerRelations = relationShips.FindAll(x => PlayerLizardRelations.Instance.IsEntityIDPlayer(x.subjectID));

            // Get player relation of the player that owns the lizard
            SocialMemory.Relationship pr = playerRelations.FirstOrDefault(x => PlayerLizardRelations.Instance.Relations.Exists(y => y.Player.abstractCreature.ID == x.subjectID));
            if (pr == default)
            {
                Globals.mls.LogError("PatchScavengerAI_DontWantToThrowAt: pr is default!");
                return true;
            }

            //Check if the lizard is owned by a player that we like
            if (pr.like > 0.6f && pr.tempLike > 0.6f)
            {
                //Globals.mls.LogInfo("PatchScavengerAI_DontWantToThrowAt: we dont want to hurt the tamed lizor");
                __result = true;
                return false;
            }
            //Globals.mls.LogInfo("PatchScavengerAI_DontWantToThrowAt: we want to hurt the tamed lizor, as rep to owner is: " + pr.like + ", " + pr.tempLike);
            return true;

        }
    }
}
