﻿using BepInEx.Logging;
using HarmonyLib;
using Menu;
using MoreSlugcats;
using RWMBF.LiteDB.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using RWDB.LiteDB.Core;
using static RWMBF.LiteDB.Core.LevelManager;
using Optional;
using Optional.Unsafe;
using RWMBF.LiteDB.Models;
namespace RWMBF.LiteDB.Patches
{
    /// <summary>
    /// DNF Feature: Damage reduction to tamed lizards
    /// </summary>
    [HarmonyPatch(typeof(Lizard), nameof(Lizard.Violence))]
    public sealed class Creature_Violence
    {
        private const float MaxDamageReduction = 0.8f; // 80%
        private static void Prefix(Lizard __instance, BodyChunk source, Vector2? directionAndMomentum, BodyChunk hitChunk, PhysicalObject.Appendage.Pos onAppendagePos, Creature.DamageType type, ref float damage, float stunBonus)
        {
            //Globals.mls.LogInfo("LIZARD_VIOLENCE: PREFIX RUN");

            // Make damage reduction apply while considering the creatures best friend
            if (!PlayerLizardRelations.Instance.IsTamedLizard(__instance.abstractCreature))
                return;

            //Globals.mls.LogInfo("LIZARD_VIOLENCE: Damage done to a tamed lizard");

            Creature friend = __instance.AI.friendTracker.friend;
            if (friend == null)
                return;

            //Globals.mls.LogInfo("LIZARD_VIOLENCE: Damage done to a tamed lizard who has a friend");

            // Ignore explosion damage when tamed
            if (type == Creature.DamageType.Explosion)
            {
                damage = 0f;
                return;
            }

            // The code below will allow a damage reduction from 10% to 50% max depending on the creatures current like/templike towards the player friend
            if (!__instance.GetRelationshipToId(friend.abstractCreature.ID, out SocialMemory.Relationship relationShip))
                return;

            //Globals.mls.LogInfo($"LIZARD_VIOLENCE: Damage done to a tamed lizard who has a friend with a relationship, id={__instance.abstractCreature.ID}");

            // TODO Find out why THE FUCK randomly we lose like and tempLike by MASSIVE AMOUNT?
            // are these not the values we are supposed to look for when considering tamed status?
            // find out wtf is going on
            float damageReduction = 0;
            float maxDamageReductionCalc = 1.0f - MaxDamageReduction;
            float minDamageReductionCalc = 0.1f;

            Option<TamedLizardEntry> tamedLizard = TamedLizardManager.Instance.GetTamedLizard(__instance.abstractCreature, SaveDataManager.Instance.WorldInstance);
            if (!tamedLizard.HasValue)
                damageReduction = Mathf.Lerp(minDamageReductionCalc, maxDamageReductionCalc, Mathf.Max(relationShip.like, relationShip.tempLike));
            else
            {
                Globals.mls.LogInfo($"LIZARD_VIOLENCE: Damage done to a LEVELED tamed lizard who has a friend with a relationship, id={__instance.abstractCreature.ID}");
                float levelModifier = Mathf.InverseLerp(1.0f, LevelManager.LevelMax, tamedLizard.ValueOrFailure().Level);
                float friendModifier = Mathf.Max(relationShip.like, relationShip.tempLike);
                float levelAndFriendModifier = Mathf.InverseLerp(0.0f, 2.0f, levelModifier + friendModifier);
                damageReduction = Mathf.Lerp(minDamageReductionCalc, maxDamageReductionCalc, levelAndFriendModifier);
            }

            // Add experience to this lizard
            TamedLizardManager.Instance.TryAddExperience(__instance.abstractCreature, SaveDataManager.Instance.WorldInstance, ExperienceGain.DamageReceived);

            // Check if a tamed lizard
            Globals.mls.LogInfo($"LIZARD_VIOLENCE: damage before={damage}");
            damage *= damageReduction;

            Globals.mls.LogInfo($"LIZARD_VIOLENCE: relationShip.Like={relationShip.like}, relationShip.tempLike={relationShip.tempLike}, damageReduction={damageReduction}");
            Globals.mls.LogInfo($"LIZARD_VIOLENCE: damage final={damage}");
        }
    }
}
