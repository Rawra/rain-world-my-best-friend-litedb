﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using RWCustom;
using RWArtifact.Extensions;
using System.Numerics;
using Optional;
using Optional.Unsafe;
using RWMBF.LiteDB.Models;
namespace RWMBF.LiteDB.Core
{
    [SuppressUnmanagedCodeSecurity]
    internal sealed class ImGUI_Always_Callbacks
    {
        public static void PresentCallback(ref IntPtr IDXGISwapChain, ref uint SyncInterval, ref uint Flags)
        {
            DrawLizardUI();
        }

        /// <summary>
        /// Draw data for every on-screen lizard
        /// </summary>
        private static void DrawLizardUI()
        {
            // Check if processManager is initialized and if the current main loop is RainWorldGame
            if (!(Custom.rainWorld.processManagerInitialized && Custom.rainWorld.processManager.currentMainLoop is RainWorldGame g))
                return;

            // Check for nulls using the null-conditional operator
            if (g.world?.game?.FirstAlivePlayer?.realizedCreature == null || g.cameras == null)
                return;

            // Get the player
            Player pl = (Player)g.world.game.FirstAlivePlayer.realizedCreature;

            // Find the camera for the player's room
            RoomCamera camera = g.cameras.FirstOrDefault(x => x.room == pl?.room);
            if (camera == default)
                return;

            // Get all tamed lizards
            foreach (var r in g.world.abstractRooms)
            {
                foreach (var c in r.creatures)
                {
                    if (c.realizedCreature == null || !(c.realizedCreature is Lizard) || !PlayerLizardRelations.Instance.IsTamedLizard(c))
                        continue;

                    // is it in the same room?
                    if (r != pl?.room.abstractRoom)
                        continue;

                    // try to retrieve the tamed lizard data
                    Option<TamedLizardEntry> tamedLizard = TamedLizardManager.Instance.GetTamedLizard(c, RWDB.LiteDB.Core.SaveDataManager.Instance.WorldInstance);
                    if (!tamedLizard.HasValue)
                        continue;
                    int lLevel = tamedLizard.ValueOrDefault().Level;
                    float lExp = tamedLizard.ValueOrDefault().Experience;
                    Vector2 creatureScreenVec2 = c.realizedCreature.mainBodyChunk.pos.WorldToScreen(ref camera);

                    ImGui.SetNextWindowPos(creatureScreenVec2);
                    //ImGui.SetNextWindowBgAlpha(0.00f);
                    ImGui.Begin($"pos_wnd_{c.ID.number}", ImGuiWindowFlags.NoBackground | ImGuiWindowFlags.NoDecoration | ImGuiWindowFlags.NoInputs | ImGuiWindowFlags.NoMouseInputs);
                    ImGui.Text($"Lv.{lLevel.ToString()}/{LevelManager.LevelMax}");
                    ImGui.Text($"XP: {lExp.ToString()}/{LevelManager.Instance.ExperienceNeededForLevel(lLevel+1)}");
                    ImGui.End();
                }
            }

        } //private static void TamedLizardsComponent()
    }
}
