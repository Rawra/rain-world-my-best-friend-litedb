﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using RWCustom;
using RWMBF.LiteDB.Models;
using Optional;
using Optional.Unsafe;
namespace RWMBF.LiteDB.Core
{
    [SuppressUnmanagedCodeSecurity]
    internal sealed class ImGUI_Menu_Callbacks
    {
        public static void PresentCallback(ref IntPtr IDXGISwapChain, ref uint SyncInterval, ref uint Flags)
        {
            if (ImGui.BeginTabItem(Plugin.PLUGIN_NAME))
            {
                TamedLizardsComponent();

                ImGui.EndTabItem();
            }
        }

        /// <summary>
        /// ImGUI Component: All tamed lizards
        /// https://pthom.github.io/imgui_manual_online/manual/imgui_manual.html
        /// </summary>
        private static void TamedLizardsComponent()
        {
            // Check if processManager is initialized and if the current main loop is RainWorldGame
            if (!(Custom.rainWorld.processManagerInitialized && Custom.rainWorld.processManager.currentMainLoop is RainWorldGame g))
            {
                ImGui.Text("Must be in-game");
                return;
            }

            ImGuiTableFlags flags = ImGuiTableFlags.SizingFixedFit | ImGuiTableFlags.RowBg | ImGuiTableFlags.Borders | ImGuiTableFlags.Resizable | ImGuiTableFlags.Reorderable | ImGuiTableFlags.Hideable;
            if (ImGui.BeginTable("tm_table", 7, flags))
            {
                ImGui.TableSetupColumn("ROOM", ImGuiTableColumnFlags.WidthFixed);
                ImGui.TableSetupColumn("INTERNAL.ID", ImGuiTableColumnFlags.WidthFixed);
                ImGui.TableSetupColumn("NAME", ImGuiTableColumnFlags.WidthStretch);
                ImGui.TableSetupColumn("HEALTH", ImGuiTableColumnFlags.WidthFixed);
                ImGui.TableSetupColumn("POSITION", ImGuiTableColumnFlags.WidthFixed);
                ImGui.TableSetupColumn("LEVEL", ImGuiTableColumnFlags.WidthFixed);
                ImGui.TableSetupColumn("EXP", ImGuiTableColumnFlags.WidthFixed);
                ImGui.TableHeadersRow();

                foreach (AbstractRoom ar in g.world.abstractRooms)
                {
                    foreach (AbstractCreature ac in ar.creatures)
                    {
                        if (ac.realizedCreature == null)
                            continue;

                        // is the creature a tamed lizard?
                        if (!PlayerLizardRelations.Instance.IsTamedLizard(ac))
                            continue;

                        ImGui.TableNextRow();
                        ImGui.TableNextColumn();
                        ImGui.Text(ar.name);

                        ImGui.TableNextColumn();
                        ImGui.Text(ac.ID.ToString());

                        if (ac.realizedCreature != null)
                        {
                            ImGui.TableNextColumn();
                            ImGui.Text(ac.realizedCreature.GetType().Name);
                            if (ac.realizedCreature.State != null && ac.realizedCreature.State is HealthState)
                            {
                                ImGui.TableNextColumn();
                                ImGui.Text(string.Format("{0:0.##}", (ac.realizedCreature.State as HealthState).health));
                            }
                            else
                            {
                                ImGui.TableNextColumn();
                                ImGui.Text("<UNINIT>");
                            }

                            if (ac.realizedCreature.mainBodyChunk != null)
                            {
                                ImGui.TableNextColumn();
                                ImGui.Text(ac.realizedCreature.mainBodyChunk.pos.ToString());
                            }
                            else
                            {
                                ImGui.TableNextColumn();
                                ImGui.Text("<UNINIT>");
                            }

                            Option<TamedLizardEntry> tamedLizard = TamedLizardManager.Instance.GetTamedLizard(ac, RWDB.LiteDB.Core.SaveDataManager.Instance.WorldInstance);
                            if (tamedLizard.HasValue)
                            {
                                ImGui.TableNextColumn();
                                ImGui.Text(tamedLizard.ValueOrDefault().Level.ToString());
                                ImGui.TableNextColumn();
                                ImGui.Text(tamedLizard.ValueOrDefault().Experience.ToString());
                            } else
                            {
                                ImGui.TableNextColumn();
                                ImGui.Text("<UNINIT>");
                                ImGui.TableNextColumn();
                                ImGui.Text("<UNINIT>");
                            }
                        }
                        else
                        {
                            ImGui.TableNextColumn();
                            ImGui.Text(ac.GetType().Name);
                            ImGui.TableNextColumn();
                            ImGui.Text("<UNINIT>");
                            ImGui.TableNextColumn();
                            ImGui.Text("<UNINIT>");
                            ImGui.TableNextColumn();
                            ImGui.Text("<UNINIT>");
                            ImGui.TableNextColumn();
                            ImGui.Text("<UNINIT>");
                        } // if (ac.realizedCreature != null)
                    }
                }
                ImGui.EndTable();
            }// if (ImGui.BeginTable("table1", 4, flags))

        } //private static void TamedLizardsComponent()
    }
}
