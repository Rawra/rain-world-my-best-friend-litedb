﻿using BepInEx;
using BepInEx.Logging;
using HarmonyLib;
using System;
using System.Text;
using RWMBF.LiteDB.Hooks;
using BepInEx.Bootstrap;
namespace RWMBF.LiteDB.Core
{
    [BepInDependency("rwartifact", BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("rwdb.litedb", BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("rwimgui", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin(PLUGIN_GUID, PLUGIN_NAME, PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        public const string PLUGIN_GUID = "rwmbf.litedb";
        public const string PLUGIN_NAME = "My Best Friend";
        public const string PLUGIN_VERSION = "1.0.0"; 

        private void Awake()
        {
            Logger.LogInfo($"Plugin {PLUGIN_NAME} is loading!");
            Globals.mls = Logger;

            var harmony = new Harmony("com.mybestfriend.patch");
            harmony.PatchAll();

            if (!DatabaseAPI.Instance.TrySessionSetup())
            {
                Logger.LogInfo($"Session setup completed with errors!");
                return;
            }

            // Hooks
            On.RainWorld.OnModsInit += RainWorld_OnModsInit;

            Logger.LogInfo($"Plugin {PLUGIN_NAME} is loaded!");
        }

        private unsafe void RainWorld_OnModsInit(On.RainWorld.orig_OnModsInit orig, RainWorld self)
        {
            try
            {
                On.Player.ctor += HooksPlayer.Player_ctor;
                On.Player.Destroy += HooksPlayer.Player_Destroy;
                On.Lizard.SpitOutOfShortCut += HooksLizard.SpitOutOfShortCut;
                On.SocialMemory.Relationship.EvenOutTemps += HooksSocialMemoryRelationship.EvenOutTemps;

                // if ImGUI-API is installed, we can use this as a hud and menu system.
                if (BepInExUtil.IsModLoaded("rwimgui"))
                {
                    Logger.LogInfo("Adding ImGUI-API callbacks...");
                    RWIMGUI.API.ImGUIAPI.AddMenuCallback(&ImGUI_Menu_Callbacks.PresentCallback);
                    RWIMGUI.API.ImGUIAPI.AddAlwaysCallback(&ImGUI_Always_Callbacks.PresentCallback);
                }

            }
            catch (Exception ex)
            {
                Globals.mls.LogError("MyBestFriend Err:" + ex);
            }
            finally
            {
                orig.Invoke(self);
            }
        }
    }
}