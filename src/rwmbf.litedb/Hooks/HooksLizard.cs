﻿using Optional;
using RWMBF.LiteDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RWMBF.LiteDB.Core.LevelManager;
using RWMBF.LiteDB.Core;
using RWDB.LiteDB.Core;
namespace RWMBF.LiteDB.Hooks
{
    internal sealed class HooksLizard
    {
        public static void SpitOutOfShortCut(On.Lizard.orig_SpitOutOfShortCut orig, Lizard self, RWCustom.IntVector2 pos, Room newRoom, bool spitOutAllSticks)
        {
            orig.Invoke(self, pos, newRoom, spitOutAllSticks);

            if (!PlayerLizardRelations.Instance.IsTamedLizard(self.abstractCreature))
                return;

            Option<TamedLizardEntry> tamedLizard = TamedLizardManager.Instance.GetTamedLizard(self.abstractCreature, SaveDataManager.Instance.WorldInstance);
            if (!tamedLizard.HasValue)
                return;

            if (!TamedLizardManager.Instance.TryAddExperience(self.abstractCreature, SaveDataManager.Instance.WorldInstance, ExperienceGain.Spit))
                Globals.mls.LogError("SpitOutOfShortCut: TryAddExperience failed");
        }

    }
}
