﻿using RWMBF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RWMBF.LiteDB.Core;
namespace RWMBF.LiteDB.Hooks
{
    public class HooksPlayer
    {
        /// <summary>
        /// Hook to keep track of all currently spawned players
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="self"></param>
        /// <param name="abstractCreature"></param>
        /// <param name="world"></param>
        public static void Player_ctor(On.Player.orig_ctor orig, Player self, AbstractCreature abstractCreature, World world)
        {
            orig.Invoke(self, abstractCreature, world);
            //Globals.mls.LogInfo("Player_ctor");

            // Only add to player relations if are an actual player...
            if (self.isNPC)
                return;

            PlayerLizardRelations.Instance.Relations.Add(new PlayerLizardRelation(self));
        }

        /// <summary>
        /// Hook to keep track of destroyed players
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="self"></param>
        public static void Player_Destroy(On.Player.orig_Destroy orig, Player self)
        {
            //Globals.mls.LogInfo("Player_Destroy");
            if (self == null)
                return;

            var plr = PlayerLizardRelations.Instance.Relations.FirstOrDefault(x => x.Player.abstractCreature.ID == self.abstractCreature.ID);
            if (plr == default)
            {
                Globals.mls.LogError($"Player_Destroy: player-lizard relation received is default!");
                orig.Invoke(self);
                return;
            }

            PlayerLizardRelations.Instance.Relations.Remove(plr);
            orig.Invoke(self);
        }
    }
}
