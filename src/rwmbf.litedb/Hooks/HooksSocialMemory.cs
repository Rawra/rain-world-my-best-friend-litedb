﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RWMBF.LiteDB.Core;
namespace RWMBF.LiteDB.Hooks
{
    internal sealed class HooksSocialMemoryRelationship
    {
        public static void EvenOutTemps(On.SocialMemory.Relationship.orig_EvenOutTemps orig, SocialMemory.Relationship self, float speed)
        {
            if (!PlayerLizardRelations.Instance.IsTamedLizard(self.subjectID))
                orig.Invoke(self, speed);

            orig.Invoke(self, speed * 0.5f);
        }

    }
}
