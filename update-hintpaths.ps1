## Small utility script to update the HintPaths inside a .csproj file with the help of a gameInstallDir file.

param (
    [string]$gameInstallDirPath = "$(Get-Location)\gameinstalldir.txt",
    [string]$csprojPath
)

if (-not (Test-Path $gameInstallDirPath)) {
    Write-Host "gameinstalldir Path not found: $gameInstallDirPath"
    return
}

if (-not (Test-Path $csprojPath)) {
    Write-Host "csproj Path not found: $csprojPath"
    return
}

function IsStringRelevant {
    param (
        [string]$str
    )

    $GameAssembly = @("UnityEngine", "Assembly-CSharp")

    foreach ($s in $GameAssembly) {
        if ($str -like "*$s*") {
            return $true
        }
    }
    return $false
}

function UpdateHintPathsString {
    param (
        [string]$oldHintPath,
        [string]$gameInstallDir,
        [string]$pathSeparator
    )

    if (-not (IsStringRelevant $(Split-Path -Leaf $oldHintPath))) {
        return $oldHintPath
    }

    $relativeHintPath = $oldHintPath -split "$pathSeparator" | Select-Object -Last 1
    $newHintPath = Join-Path $gameInstallDir $relativeHintPath

    return $newHintPath
}

Write-Host "Game Install Directory: $gameInstallDirPath"
$gameInstallDir = Get-Content $gameInstallDirPath

# Get the last part of the path, we will use it for separating strings later
$stringSeparator = (Split-Path -Leaf $gameInstallDir)

# Load the XML document
$xmlDoc = New-Object System.Xml.XmlDocument
$xmlDoc.Load($csprojPath)

# Find all <Reference> nodes
$referenceNodes = $xmlDoc.SelectNodes("//Reference")

if ($null -eq $referenceNodes) {
    return
}

foreach ($referenceNode in $referenceNodes) {
    # Get the HintPath node
    $hintPathNode = $referenceNode.SelectSingleNode("HintPath")

    if ($null -eq $hintPathNode) {
        continue
    }

    $hintPathNode.InnerText = UpdateHintPathsString $hintPathNode.InnerText $gameInstallDir $stringSeparator
}

$xmlDoc.Save($csprojPath)
Write-Host "CSProj file updated."
