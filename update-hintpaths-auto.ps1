$csprojPaths = @(
    ".\src\rain-world-imgui-api\rain-world-imgui-api.csproj",
    ".\src\rain-world-imgui-api-example\rain-world-imgui-api-example.csproj"
)

foreach ($csprojPath in $csprojPaths) {
    .\update-hintpaths.ps1 -csprojPath $csprojPath
}
